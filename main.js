var router = new VueRouter();

var mainComponent = Vue.extend({
    components: {
        'bill-component' : billComponent
    },
    template: '<bill-component></bill-component>',
    data: function() {
        return {
            billsPay: [
                {date_due: '20/08/2016', name: 'Conta de Luz', value: 101.87, done: true},
                {date_due: '21/08/2016', name: 'Conta de Água', value: 30.54, done: true},
                {date_due: '22/08/2016', name: 'Conta de Telefone', value: 50.01, done: true},
                {date_due: '23/08/2016', name: 'Supermercado', value: 287.32, done: true},
                {date_due: '24/08/2016', name: 'Cartão de Crédito', value: 326.08, done: true},
                {date_due: '25/08/2016', name: 'Empréstimo', value: 987.98, done: false},
                {date_due: '26/08/2016', name: 'Gasolina', value: 50.00, done: false}
            ],
            billsReceive: [
                {date_due: '20/08/2016', name: 'Tesouro Direto', value: 1000.99, done: false},
                {date_due: '21/08/2016', name: 'Freela', value: 1500.00, done: false},
                {date_due: '22/08/2016', name: 'Google AdSense', value: 230.76, done: false},
                {date_due: '23/08/2016', name: 'Salario', value: 5999.99, done: true},
                {date_due: '24/08/2016', name: 'Mercado Livre', value: 128.43, done: true}
            ]
        };
    }
})

router.map({
    '/bill-pays' : {
        component: billPayComponent,
        subRoutes: {
            '/': { //rota padrão
                name: 'bill-pay.list',
                component: billPayListComponent // pode usar window.billListComponente também'
            },
            '/create': {
                name: 'bill-pay.create',
                component: billPayCreateComponent
            },
            '/:index/update': {
                name: 'bill-pay.update',
                component: billPayCreateComponent
            }
        }
    },
    '/bill-receives': {
        component: billReceiveComponent,
        subRoutes: {
            '/': { //rota padrão
                name: 'bill-receive.list',
                component: billReceiveListComponent // pode usar window.billListComponente também'
            },
            '/create': {
                name: 'bill-receive.create',
                component: billReceiveCreateComponent
            },
            '/:index/update': {
                name: 'bill-receive.update',
                component: billReceiveCreateComponent
            }
        }
    },
    '/': {
        name: 'dashboard',
        component: dashboardComponent
    },
    '*' :{
        component: billPayComponent
    }
    
});

router.start({
    components: {
        'bill-pay-component': mainComponent
    }
}, '#app');


router.redirect({
    '*' : '/bill-pays'
})
