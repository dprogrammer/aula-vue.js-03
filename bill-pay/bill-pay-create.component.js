window.billPayCreateComponent = Vue.extend({
    template: `
        <form name="form" @submit.prevent="submit">
                <label>Vencimento:</label>
                <input type="text" v-model="bill.date_due" />
                <br><br>
                <label>Nome:</label>
                <select v-model="bill.name">
                    <!--<option v-for="o in names" value="{{ o }}">{{ o }}</option>-->
                    <!--<option v-for="o in names" v-bind:value="o">{{ o }}</option>-->
                    <option v-for="o in names" :value="o">{{ o }}</option> <!-- property binding -->
                </select>    
                <br><br>
                <label>Valor:</label>
                <input type="text" v-model="bill.value" />
                <br><br>
                <input type="submit" value="Enviar" />
            </form>
    `,
    //props: ['bill'], não precisa mais da propriedade. usando events // no componente usa assim -> v-bind:bill="bill" -> é para passar o valor do componente estando no html'
    data: function() {
        return {
            formType: 'insert',
            names: [
                'Conta de Luz',
                'Conta de Água',
                'Conta de Telefone',
                'Supermercado',
                'Cartão de Crédito',
                'Empréstimo',
                'Gasolina'
            ],
            bill: {
                date_due: '',
                name: '',
                value: 0,
                done: false
            }
        };
    },
    created: function(){
        console.log(this.$route.name);
        if (this.$route.name == 'bill-pay.update') {
            this.formType = 'update';

            var parametroIndex = this.$route.params.index; // params é um objeto e index é o nome do paramâmetro na rota
            this.getBill(parametroIndex);

            return;
        }
    },
    methods: {
        submit: function () {

            console.log(this.formType);
            
            if (this.formType == 'insert') {
         
                this.$root.$children[0].billsPay.push(this.bill);

                console.log('bill create ADD');

                this.bill = {
                    date_due: '',
                    name: '',
                    value: 0,
                    done: false
                }

            } 

            this.$router.go({name: 'bill-pay.list'});
        },
        getBill: function(index) {
            var bills = this.$root.$children[0].billsPay;
            this.bill = bills[index];
        }
    }
});
Vue.component('bill-pay-create-component', billPayCreateComponent);