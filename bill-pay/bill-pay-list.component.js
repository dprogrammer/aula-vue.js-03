window.billPayListComponent = Vue.extend({
    template: `
        <style type="text/css">
            .pago {
                color: green;
            }
            .nao-pago {
                color: red;
            }
        </style>
        <table border="1" cellpadding="10">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Vencimento</th>
                    <th>Nome</th>
                    <th>Valor</th>
                    <th>Paga?</th>
                    <th>Ações</th>
                </tr>
            </thead>
            <tbody>
                <tr v-for="(index,o) in bills"> <!-- primeiro exemplo = o in bills" -->
                    <td>{{ index + 1 }}</td>
                    <td>{{ o.date_due }}</td>
                    <td>{{ o.name }}</td>
                    <td>{{ o.value | currency 'R$ ' }}</td>
                    <td class="backg-col" :class="{'pago': o.done, 'nao-pago': !o.done}">
                        <input type="checkbox" v-model="o.done" /> {{ o.done | doneLabel }}
                    </td>
                    <td>
                        <!--<a href="#" @click.prevent="loadBill(o)">Editar</a> | <a href="#"  @click.prevent="deleteBill(o)">Excluir</a>-->
                        <a v-link="{ name: 'bill-pay.update', params: {index: index} }">Editar</a> | <a href="#"  @click.prevent="deleteBill(o)">Excluir</a>
                    </td>
                </tr>
            </tbody>
        </table>

        <br>
        Total a pagar: {{ totalPay }}
    `,
    data: function () {
        return {
            bills: this.$root.$children[0].billsPay // root é o componente Pai'
        };
    },
    methods: {
        deleteBill: function(bill) {
            //http://stackoverflow.com/a/12582246
            var x = confirm("Deseja confirma a exclusão da conta " + bill.name + "?");
                
            if (x)
                this.$root.$children[0].billsPay.$remove(bill);
            
                //http://stackoverflow.com/a/35459981
        }
    },
    computed: {
        totalPay: function () {
            // novo status para o exercício
            var totalTemp = 0;
            var billListComponent = this.$root.$children[0].billsPay

            console.log("tou pay");

            for ( var i in billListComponent ) {
               totalTemp += billListComponent[i].value;

               console.log("valor pay: " + billListComponent[i].value);
            }

            return totalTemp;
        }
    }
});
Vue.component('bill-pay-list-component', billPayListComponent);