window.billPayMenuComponent = Vue.extend({
    template: `
        <nav>
            <ul >
                <li v-for="o in menus">
                    <!--<a href="javascript:void(0)" @click.prevent="showView($event, o.id)">{{ o.name }}</a>-->
                    <!--<a v-link="{path: o.url}">{{ o.name }}</a>-->
                    <a v-link="{name: o.routeName}">{{ o.name }}</a>
                </li>
            </ul>
        </nav>
    `,
    data: function () {
        return {
            menus: [
                //{id: 0, name: "Listar Contas", url: '/bills'},
                //{id: 1, name: "Criar Conta", url: '/bill/create'}
                {id: 0, name: "Listar Contas", routeName: 'bill-pay.list'},
                {id: 1, name: "Criar Conta", routeName: 'bill-pay.create'}
            ],
        };
    }
});
Vue.component('menu-component', billPayMenuComponent);