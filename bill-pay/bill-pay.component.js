window.billPayComponent = Vue.extend({
    template: ` 
        <style type="text/css">
        .backg-col {
            background-color: #CCCCCC;
        }
        .statusVerde {
            color: green;
        }
        .statusVermelho {
            color: red;
        }
        .statusCinza {
            color: #CCCCCC;
        }
    </style>
        <h1>{{ title }}</h1>
        <h3 :class="{'statusCinza':status === false, 'statusVerde':status === 0, 'statusVermelho':status > 0}">{{ status | situacaoContasText }}</h3>

        <menu-component></menu-component>

        <router-view></router-view>
 
    `,
    data: function(){ // para que o componente possa ser reaproveita, já que cada um terá o seu data
        return {
            title: "Contas a pagar"
        }; // fechando o data
    },
    computed: {
        status: function () {
            // novo status para o exercício
            var countPagos = 0;
            var countNaoPagos = 0;
            var statusTemp = false;
            var billListComponent = this.$root.$children[0].billsPay

            for ( var i in billListComponent ) {
               if (!billListComponent[i].done ) {
                   countNaoPagos++;
                   console.log("valor i: " + i + " - valor count: " + countNaoPagos);
               } else {
                   countPagos++;
                   console.log("valor i: " + i + " - valor count: " + countPagos);
               }
            }

            if (countPagos > 0 && countNaoPagos == 0) {
                statusTemp = 0;
            } else if (countNaoPagos > 0) {
                statusTemp = countNaoPagos;
            }

            return statusTemp;
        }
    }
});

Vue.component('bill-pay-component', billPayComponent); // para registrar