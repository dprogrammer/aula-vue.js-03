// Instrutor -> Lembrando que agora precisamos que os filtros sejam carregados antes da instrução "new Vue", para que o Vue já possa usa-los de cara.

Vue.filter('situacaoContasText', function(value){
    if (value === false) { // se colocar == então 0 == false dá true'
        return "Nenhuma conta cadastrada"
    } else if (value === 0) {
        return "Nenhuma conta a pagar"
    } else {
        return "Existem "+ value +" contas a serem pagas"
    }
})

Vue.filter('situacaoContasReceiveText', function(value){
    if (value === false) { // se colocar == então 0 == false dá true'
        return "Nenhuma conta cadastrada"
    } else if (value === 0) {
        return "Nenhuma conta a receber"
    } else {
        return "Existem "+ value +" contas a serem recebidas"
    }
})

Vue.filter('doneLabel', function(value){
    if (!value) {
        return "Não Paga"
    } else {
        return "Paga"
    }
})

Vue.filter('doneReceiveLabel', function(value){
    if (!value) {
        return "Não Recebida"
    } else {
        return "Recebida"
    }
})