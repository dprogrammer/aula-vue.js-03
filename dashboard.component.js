window.dashboardComponent = Vue.extend({
    template: ` 
        <style type="text/css">
        .backg-col {
            background-color: #CCCCCC;
        }
        .statusVerde {
            color: green;
        }
        .statusVermelho {
            color: red;
        }
        .statusCinza {
            color: #CCCCCC;
        }
    </style>
        <h1>{{ title }}</h1>
 
       <p>Total a Receber: <span class="statusVerde">{{ totalReceive = totalReceiveDash | currency 'R$ '}}</span></p>
       <p>Total a Pagar: <span class="statusVermelho">{{ totalPay = totalPayDash | currency 'R$ '}}</span></p>
       <p>Saldo: <span :class="{'statusVerde': totalReceive - totalPay >= 0,  'statusVermelho': totalReceive - totalPay < 0}">{{ totalReceive - totalPay | currency 'R$ '}}</span></p>
    `,
    data: function(){ // para que o componente possa ser reaproveita, já que cada um terá o seu data
        return {
            title: "Dashboard",
            totalReceive: 0,
            totalPay: 0
        }; // fechando o data
    },
    computed: {
        totalReceiveDash: function () {
            // novo status para o exercício
            var totalTemp = 0;
            var billListComponent = this.$root.$children[0].billsReceive

            console.log("tou receiver");

            for ( var i in billListComponent ) {
               totalTemp += billListComponent[i].value;

               console.log("valor receiver: " + billListComponent[i].value);
            }

            return totalTemp;
        },
        totalPayDash: function () {
            // novo status para o exercício
            var totalTemp = 0;
            var billListComponent = this.$root.$children[0].billsPay

            for ( var i in billListComponent ) {
               totalTemp += billListComponent[i].value;

               console.log("valor pay: " + billListComponent[i].value);
            }

            return totalTemp;
        }
    }
});

Vue.component('dashboard-component', dashboardComponent); // para registrar