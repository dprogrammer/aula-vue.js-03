window.billReceiveCreateComponent = Vue.extend({
    template: `
        <form name="form" @submit.prevent="submit">
                <label>Recebimento:</label>
                <input type="text" v-model="bill.date_due" />
                <br><br>
                <label>Nome:</label>
                <select v-model="bill.name">
                    <option v-for="o in names" :value="o">{{ o }}</option> 
                </select>    
                <br><br>
                <label>Valor:</label>
                <input type="text" v-model="bill.value" />
                <br><br>
                <input type="submit" value="Enviar" />
            </form>
    `,
    //props: ['bill'], não precisa mais da propriedade. usando events // no componente usa assim -> v-bind:bill="bill" -> é para passar o valor do componente estando no html'
    data: function() {
        return {
            formType: 'insert',
            names: [
                'Tesouro Direto',
                'Mercado Livre',
                'Freela',
                'Salário',
                'Google AdSense'
            ],
            bill: {
                date_due: '',
                name: '',
                value: 0,
                done: false
            }
        };
    },
    created: function(){
        console.log(this.$route.name);
        if (this.$route.name == 'bill-receive.update') {
            this.formType = 'update';

            var parametroIndex = this.$route.params.index; // params é um objeto e index é o nome do paramâmetro na rota
            this.getBill(parametroIndex);

            return;
        }
    },
    methods: {
        submit: function () {

            console.log(this.formType);
            
            if (this.formType == 'insert') {
         
                this.$root.$children[0].billsReceive.push(this.bill);

                console.log('bill Receive create ADD');

                this.bill = {
                    date_due: '',
                    name: '',
                    value: 0,
                    done: false
                }

            } 

            this.$router.go({name: 'bill-receive.list'});
        },
        getBill: function(index) {
            var bills = this.$root.$children[0].billsReceive;
            this.bill = bills[index];
        }
    }
});
Vue.component('bill-receive-create-component', billReceiveCreateComponent);